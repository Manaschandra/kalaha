package ai;

import ai.Global;
import java.io.*;
import java.net.*;
import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import kalaha.*;

/**
 * This is the main class for your Kalaha AI bot. Currently
 * it only makes a random, valid move each turn.
 * 
 * @author Manas
 */
public class AIClient implements Runnable
{
    private int player;
    private JTextArea text;
    
    private PrintWriter out;
    private BufferedReader in;
    private Thread thr;
    private Socket socket;
    private boolean running;
    private boolean connected;
    	
    /**
     * Creates a new client.
     */
    public AIClient()
    {
	player = -1;
        connected = false;
        
        //This is some necessary client stuff. You don't need
        //to change anything here.
        initGUI();
	
        try
        {
            addText("Connecting to localhost:" + KalahaMain.port);
            socket = new Socket("localhost", KalahaMain.port);
            out = new PrintWriter(socket.getOutputStream(), true);
            in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            addText("Done");
            connected = true;
        }
        catch (Exception ex)
        {
            addText("Unable to connect to server");
            return;
        }
    }
    
    /**
     * Starts the client thread.
     */
    public void start()
    {
        //Don't change this
        if (connected)
        {
            thr = new Thread(this);
            thr.start();
        }
    }
    
    /**
     * Creates the GUI.
     */
    private void initGUI()
    {
        //Client GUI stuff. You don't need to change this.
        JFrame frame = new JFrame("My AI Client");
        frame.setLocation(Global.getClientXpos(), 445);
        frame.setSize(new Dimension(420,250));
        frame.getContentPane().setLayout(new FlowLayout());
        
        text = new JTextArea();
        JScrollPane pane = new JScrollPane(text);
        pane.setPreferredSize(new Dimension(400, 210));
        
        frame.getContentPane().add(pane);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        frame.setVisible(true);
    }
    
    /**
     * Adds a text string to the GUI textarea.
     * 
     * @param txt The text to add
     */
    public void addText(String txt)
    {
        //Don't change this
        text.append(txt + "\n");
        text.setCaretPosition(text.getDocument().getLength());
    }
    
    /**
     * Thread for server communication. Checks when it is this
     * client's turn to make a move.
     */
    public void run()
    {
        String reply;
        running = true;
        
        try
        {
            while (running)
            {
                //Checks which player you are. No need to change this.
                if (player == -1)
                {
                    out.println(Commands.HELLO);
                    reply = in.readLine();

                    String tokens[] = reply.split(" ");
                    player = Integer.parseInt(tokens[1]);
                    
                    addText("I am player " + player);
                }
                
                //Check if game has ended. No need to change this.
                out.println(Commands.WINNER);
                reply = in.readLine();
                if(reply.equals("1") || reply.equals("2") )
                {
                    int w = Integer.parseInt(reply);
                    if (w == player)
                    {
                        addText("I won!");
                    }
                    else
                    {
                        addText("I lost...");
                    }
                    running = false;
                }
                if(reply.equals("0"))
                {
                    addText("Even game!");
                    running = false;
                }

                //Check if it is my turn. If so, do a move
                out.println(Commands.NEXT_PLAYER);
                reply = in.readLine();
                if (!reply.equals(Errors.GAME_NOT_FULL) && running)
                {
                    int nextPlayer = Integer.parseInt(reply);

                    if(nextPlayer == player)
                    {
                        out.println(Commands.BOARD);
                        String currentBoardStr = in.readLine();
                        boolean validMove = false;
                        while (!validMove)
                        {
                            long startT = System.currentTimeMillis();
                            //This is the call to the function for making a move.
                            //You only need to change the contents in the getMove()
                            //function.
                            GameState currentBoard = new GameState(currentBoardStr);
                            int cMove = getMove(currentBoard);
                            
                            //Timer stuff
                            long tot = System.currentTimeMillis() - startT;
                            double e = (double)tot / (double)1000;
                            
                            out.println(Commands.MOVE + " " + cMove + " " + player);
                            reply = in.readLine();
                            if (!reply.startsWith("ERROR"))
                            {
                                validMove = true;
                                addText("Made move " + cMove + " in " + e + " secs");
                            }
                        }
                    }
                }
                
                //Wait
                Thread.sleep(100);
            }
	}
        catch (Exception ex)
        {
            running = false;
        }
        
        try
        {
            socket.close();
            addText("Disconnected from server");
        }
        catch (Exception ex)
        {
            addText("Error closing connection: " + ex.getMessage());
        }
    }
    
    /**
     * This is the method that makes a move each time it is your turn.
     * Here you need to change the call to the random method to your
     * Minimax search.
     * 
     * @param currentBoard The current board state
     * @return Move to make (1-6)
     */
    public int getMove(GameState currentBoard)
    {
        long t1 = System.currentTimeMillis();
        int md;
        int bestmove=0;
        int MAX_DEPTH=12;
        
        String s = currentBoard.toString();
        String[] token = s.split(";");
        
        int[] board = new int [14];
        int nextplayer;
        int i;
        
        for(i=0;i<14;i++)
        {
            board[i] = Integer.parseInt(token[i]);
        }
        nextplayer = Integer.parseInt(token[14]);
        int move=0;
        if(nextplayer==1 && board[7]<=3)
            move= openingBook(board,nextplayer);
        else if(nextplayer==2 && board[0]<2)
            move= openingBook(board,nextplayer);
        if(move!=0)
        { return move;}
        
         
	for ( md=1; md <= MAX_DEPTH ;++md ) //iterativeDeepening
	{
        long t2=System.currentTimeMillis();
		int alpha=-1000;
		int beta=1000;
                if((t2-t1)<5000)
                    bestmove = minimax(currentBoard,md,alpha,beta,t1);
                else break;
	}
        if((bestmove==0)||(bestmove>6))
        {
            bestmove = getRandom();
            return bestmove;
        }
        return bestmove;
    }
    
	//Opening Book: contains 100 first possible moves
    public int openingBook(int[] board,int nextplayer) 
    {
        if(nextplayer==1)
        {
            if(board[7]==0)//first move--1
                return 1;
            else if(board[7]==1 && board[1]==0)//first move's free turn--2
                return 2;
            //second moves--6
            else if(board[1]==1 && board[2]==0 && board[3]==8 && board[4]==8 && board[5]==8 && board[6]==8)
                return 1;
            else if(board[1]==1 && board[2]==1 && board[3]==8 && board[4]==8 && board[5]==8 && board[6]==8 && board[9]==0)
                return 6;
            else if(board[1]==1 && board[2]==1 && board[3]==8 && board[4]==8 && board[5]==8 && board[6]==8)
                return 3;
            else if(board[1]==1 && board[2]==1 && board[3]==9 && board[4]==8 && board[5]==8 && board[6]==8)
                return 3;
            else if(board[1]==1 && board[2]==1 && board[3]==9 && board[4]==9 && board[5]==8 && board[6]==8)
                return 6;
            else if(board[1]==1 && board[2]==1 && board[3]==9 && board[4]==9 && board[5]==9 && board[6]==8)
                return 5;
            
            //third moves--
            //after 2 --6
            else if(board[1]==3 && board[2]==2 && board[3]==8 && board[4]==8 && board[5]==8 && board[6]==0)
                return 3;
            else if(board[1]==2 && board[2]==1 && board[3]==8 && board[4]==8 && board[5]==8 && board[6]==0)
                return 4;
            else if(board[1]==3 && board[2]==2 && board[3]==9 && board[4]==9 && board[5]==8 && board[6]==0 && board[8]==8)
                return 3;
            else if(board[1]==3 && board[2]==2 && board[3]==9 && board[4]==9 && board[5]==9 && board[6]==0)
                return 4;
            else if(board[1]==3 && board[2]==2 && board[3]==9 && board[4]==9 && board[5]==9 && board[6]==1)
                return 6;
            else if(board[1]==3 && board[2]==2 && board[3]==9 && board[4]==9 && board[5]==9 && board[6]==1)
                return 1;
            //after 3 --6
            else if(board[1]==2 && board[2]==2 && board[3]==0 && board[4]==9 && board[5]==9 && board[6]==9 && board[10]==2)
                return 1;
            else if(board[1]==2 && board[2]==2 && board[3]==1 && board[4]==9 && board[5]==9 && board[6]==9 && board[10]==2)
                return 5;
            else if(board[1]==1 && board[2]==1 && board[3]==0 && board[4]==9 && board[5]==9 && board[6]==9 && board[10]==0)
                return 2;
            else if(board[1]==2 && board[2]==2 && board[3]==1 && board[4]==10 && board[5]==10 && board[6]==9 && board[11]==0)
                return 6;
            else if(board[1]==2 && board[2]==2 && board[3]==1 && board[4]==10 && board[5]==10 && board[6]==9)
                return 5;
            else if(board[1]==2 && board[2]==2 && board[3]==1 && board[4]==10 && board[5]==10 && board[6]==10 && board[10]==1)
                return 2;
            //after 4 --6
            else if(board[1]==2 && board[2]==2 && board[3]==0 && board[4]==9 && board[5]==9 && board[6]==9)
                return 6;
            else if(board[1]==2 && board[2]==2 && board[3]==1 && board[4]==9 && board[5]==9 && board[6]==9 && board[9]==0)
                return 5;
            else if(board[1]==2 && board[2]==2 && board[3]==1 && board[4]==9 && board[5]==9 && board[6]==9)
                return 6;
            else if(board[1]==1 && board[2]==1 && board[3]==0 && board[4]==9 && board[5]==9 && board[6]==9)
                return 5;
            else if(board[1]==2 && board[2]==2 && board[3]==1 && board[4]==10 && board[5]==10 && board[6]==10 && board[12]==0)
                return 4;
            else if(board[1]==2 && board[2]==2 && board[3]==1 && board[4]==9 && board[5]==9 && board[6]==9)
                return 4;
            //after 5 --6
            else if(board[1]==3 && board[2]==2 && board[3]==9 && board[4]==9 && board[5]==8 && board[6]==0)
                return 4;
            else if(board[1]==3 && board[2]==2 && board[3]==10 && board[4]==9 && board[5]==8 && board[6]==0 && board[9]==0)
                return 3;
            else if(board[1]==3 && board[2]==2 && board[3]==10 && board[4]==9 && board[5]==8 && board[6]==0)
                return 5;
            else if(board[1]==3 && board[2]==2 && board[3]==10 && board[4]==10 && board[5]==8 && board[6]==0)
                return 4;
            else if(board[1]==2 && board[2]==1 && board[3]==9 && board[4]==9 && board[5]==8 && board[6]==0)
                return 4;
            else if(board[1]==3 && board[2]==2 && board[3]==10 && board[4]==10 && board[5]==9 && board[6]==1)
                return 2;
            //after 6 --10
            else if(board[1]==3 && board[2]==2 && board[3]==9 && board[4]==9 && board[5]==0 && board[6]==9 && board[13]==2)
                return 3;
            else if(board[1]==3 && board[2]==2 && board[3]==10 && board[4]==9 && board[5]==0 && board[6]==9 && board[9]==0 && board[13]==2)
                return 2;
            else if(board[1]==3 && board[2]==2 && board[3]==10 && board[4]==9 && board[5]==0 && board[6]==9 && board[13]==2)
                return 2;
            else if(board[1]==3 && board[2]==2 && board[3]==10 && board[4]==10 && board[5]==0 && board[6]==9 && board[13]==2)
                return 3;
            else if(board[1]==3 && board[2]==2 && board[3]==10 && board[4]==10 && board[5]==1 && board[6]==9 && board[13]==2)
                return 6;
            
            else if(board[1]==3 && board[2]==2 && board[3]==9 && board[4]==9 && board[5]==0 && board[6]==9)
                return 6;
            else if(board[1]==3 && board[2]==2 && board[3]==10 && board[4]==9 && board[5]==0 && board[6]==9 && board[9]==0 && board[13]==2)
                return 3;
            else if(board[1]==3 && board[2]==2 && board[3]==10 && board[4]==9 && board[5]==0 && board[6]==9 && board[13]==2)
                return 3;
            else if(board[1]==3 && board[2]==2 && board[3]==10 && board[4]==10 && board[5]==0 && board[6]==9)
                return 3;
            else if(board[1]==3 && board[2]==2 && board[3]==10 && board[4]==10 && board[5]==1 && board[6]==9)
                return 6;
            //fourth moves --
            //move 3--6---5
            else if(board[1]==3 && board[2]==1 && board[3]==2 && board[4]==11 && board[5]==10 && board[6]==10)
                return 3;
            else if(board[1]==3 && board[2]==1 && board[3]==3 && board[4]==11 && board[5]==10 && board[6]==10)
                return 3;
            else if(board[1]==2 && board[2]==0 && board[3]==2 && board[4]==11 && board[5]==10 && board[6]==10)
                return 3;
            else if(board[1]==3 && board[2]==1 && board[3]==2 && board[4]==12 && board[5]==11 && board[6]==10)
                return 2;
            else if(board[1]==3 && board[2]==1 && board[3]==2 && board[4]==12 && board[5]==11 && board[6]==10)
                return 2;
            //move 6--2---5
            else if(board[1]==4 && board[2]==1 && board[3]==11 && board[4]==10 && board[5]==0 && board[6]==9 && board[9]==0)
                return 1;
            else if(board[1]==4 && board[2]==1 && board[3]==12 && board[4]==11 && board[5]==1 && board[6]==9 && board[9]==0)
                return 3;
            else if(board[1]==4 && board[2]==1 && board[3]==12 && board[4]==11 && board[5]==1 && board[6]==9 && board[9]==0)
                return 3;
            else if(board[1]==4 && board[2]==1 && board[3]==12 && board[4]==11 && board[5]==1 && board[6]==10 && board[9]==0)
                return 1;
            else if(board[1]==4 && board[2]==0 && board[3]==11 && board[4]==10 && board[5]==0 && board[6]==9 && board[9]==0)
                return 3;
            //move 6--3---5
            else if(board[1]==4 && board[2]==1 && board[3]==11 && board[4]==10 && board[5]==0 && board[6]==9)
                return 1;
            else if(board[1]==4 && board[2]==1 && board[3]==12 && board[4]==11 && board[5]==1 && board[6]==9)
                return 3;
            else if(board[1]==4 && board[2]==1 && board[3]==12 && board[4]==11 && board[5]==1 && board[6]==9)
                return 3;
            else if(board[1]==4 && board[2]==1 && board[3]==12 && board[4]==11 && board[5]==1 && board[6]==10)
                return 1;
            else if(board[1]==4 && board[2]==0 && board[3]==11 && board[4]==10 && board[5]==0 && board[6]==9)
                return 3;
            else return 0;
            
        }
        else
        {
            if(board[8]==7 && board[9]==7 && board[10]==6 && board[11]==6 && board[12]==6 && board[13]==6 && board[1]==0)
                return 4;
            else if(board[8]==8 && board[9]==8 && board[10]==8 && board[11]==9 && board[12]==1 && board[13]==1)
                return 1;
            else if(board[8]==1 && board[9]==10 && board[10]==9 && board[11]==3 && board[12]==10 && board[13]==9)
                return 4;
            else if(board[8]==1 && board[9]==10 && board[10]==9 && board[11]==0 && board[12]==11 && board[13]==10)
                return 6;
            else if(board[8]==7 && board[9]==7 && board[10]==7 && board[11]==6 && board[12]==6 && board[13]==6 && board[1]==0)
                return 3;
            else if(board[8]==7 && board[9]==7 && board[10]==7 && board[11]==7 && board[12]==6 && board[13]==6 && board[1]==0)
                return 4;
            else if(board[8]==7 && board[9]==7 && board[10]==7 && board[11]==7 && board[12]==7 && board[13]==6 && board[1]==0)
                return 4;
            else if(board[8]==7 && board[9]==7 && board[10]==7 && board[11]==7 && board[12]==7 && board[13]==7 && board[1]==0)
                return 4;
            else if(board[8]==7 && board[9]==7 && board[10]==6 && board[11]==6 && board[12]==6 && board[13]==0 && board[1]==0)
                return 1;
            else if(board[8]==7 && board[9]==7 && board[10]==7 && board[11]==7 && board[12]==7 && board[13]==7)
                return 1;
            else if(board[8]==8 && board[9]==8 && board[10]==7 && board[11]==7 && board[12]==7 && board[13]==0)
                return 5;
            else if(board[8]==8 && board[9]==8 && board[10]==7 && board[11]==7 && board[12]==7 && board[13]==1)
                return 1;
            else if(board[8]==8 && board[9]==8 && board[10]==7 && board[11]==7 && board[12]==7 && board[13]==1 && board[5]==0)
                return 6;
            else if(board[8]==8 && board[9]==8 && board[10]==7 && board[11]==7 && board[12]==7 && board[13]==1)
                return 6;
            
            
            else if(board[8]==7 && board[9]==6 && board[10]==6 && board[11]==6 && board[12]==6 && board[13]==6)
                return 6;
            else if(board[8]==7 && board[9]==7 && board[10]==6 && board[11]==6 && board[12]==6 && board[13]==6)
                return 2;
            else if(board[8]==7 && board[9]==7 && board[10]==7 && board[11]==6 && board[12]==6 && board[13]==6)
                return 6;
            else if(board[8]==7 && board[9]==7 && board[10]==7 && board[11]==7 && board[12]==6 && board[13]==6)
                return 1;
            else if(board[8]==7 && board[9]==7 && board[10]==7 && board[11]==7 && board[12]==7 && board[13]==6)
                return 5;
            
            else if(board[8]==8 && board[9]==0 && board[10]==7 && board[11]==7 && board[12]==7 && board[13]==7)
                return 1;
            else if(board[8]==8 && board[9]==1 && board[10]==7 && board[11]==7 && board[12]==7 && board[13]==7)
                return 1;
            else if(board[8]==8 && board[9]==1 && board[10]==8 && board[11]==8 && board[12]==7 && board[13]==7)
                return 1;
            else if(board[8]==8 && board[9]==1 && board[10]==8 && board[11]==8 && board[12]==8 && board[13]==7)
                return 1;
            else if(board[8]==8 && board[9]==1 && board[10]==8 && board[11]==8 && board[12]==8 && board[13]==8)
                return 1;
            
            else if(board[8]==8 && board[9]==7 && board[10]==7 && board[11]==6 && board[12]==6 && board[13]==0)
                return 5;
            else if(board[8]==8 && board[9]==8 && board[10]==7 && board[11]==6 && board[12]==6 && board[13]==0)
                return 5;
            else if(board[8]==8 && board[9]==8 && board[10]==8 && board[11]==6 && board[12]==6 && board[13]==0)
                return 5;
            else if(board[8]==8 && board[9]==8 && board[10]==8 && board[11]==7 && board[12]==6 && board[13]==0)
                return 5;
            else if(board[8]==8 && board[9]==8 && board[10]==8 && board[11]==7 && board[12]==7 && board[13]==0)
                return 5;
            else if(board[8]==8 && board[9]==8 && board[10]==8 && board[11]==7 && board[12]==7 && board[13]==1)
                return 5;
            
            else if(board[8]==1 && board[9]==8 && board[10]==8 && board[11]==8 && board[12]==7 && board[13]==7)
                return 1;
            else if(board[8]==1 && board[9]==9 && board[10]==8 && board[11]==8 && board[12]==7 && board[13]==7)
                return 1;
            else if(board[8]==1 && board[9]==9 && board[10]==9 && board[11]==8 && board[12]==7 && board[13]==7)
                return 1;
            else if(board[8]==1 && board[9]==9 && board[10]==9 && board[11]==9 && board[12]==8 && board[13]==8)
                return 1;

			 else if(board[8]==3 && board[9]==1 && board[10]==2 && board[11]==11 && board[12]==10 && board[13]==10)
                return 3;
            else if(board[8]==3 && board[9]==1 && board[10]==3 && board[11]==11 && board[12]==10 && board[13]==10)
                return 3;
            else if(board[8]==2 && board[9]==0 && board[10]==2 && board[11]==11 && board[12]==10 && board[13]==10)
                return 3;
            else if(board[8]==3 && board[9]==1 && board[10]==2 && board[11]==12 && board[12]==11 && board[13]==10)
                return 2;
            else if(board[8]==3 && board[9]==1 && board[10]==2 && board[11]==12 && board[12]==11 && board[13]==10)
                return 2;
            
            else if(board[8]==4 && board[9]==1 && board[10]==11 && board[11]==10 && board[12]==0 && board[13]==9)
                return 1;
            else if(board[8]==4 && board[9]==1 && board[10]==12 && board[11]==11 && board[12]==1 && board[13]==9)
                return 3;
            else if(board[8]==4 && board[9]==1 && board[10]==12 && board[11]==11 && board[12]==1 && board[13]==9)
                return 3;
            else if(board[8]==4 && board[9]==1 && board[10]==12 && board[11]==11 && board[12]==1 && board[13]==10)
                return 1;
            else if(board[8]==4 && board[9]==0 && board[10]==11 && board[11]==10 && board[12]==0 && board[13]==9)
                return 3;
            
            else if(board[8]==4 && board[9]==1 && board[10]==11 && board[11]==10 && board[12]==0 && board[13]==9)
                return 1;
            else if(board[8]==4 && board[9]==1 && board[10]==12 && board[11]==11 && board[12]==1 && board[13]==9)
                return 3;
            else if(board[8]==4 && board[9]==1 && board[10]==12 && board[11]==11 && board[12]==1 && board[13]==9)
                return 3;
            else if(board[8]==4 && board[9]==1 && board[10]==12 && board[11]==11 && board[12]==1 && board[13]==10)
                return 1;
            else if(board[8]==4 && board[9]==0 && board[10]==11 && board[11]==10 && board[12]==0 && board[13]==9)
                return 3;
        }
        return 0;
    }

	//minimax
    public int minimax(GameState board,int md,int alpha,int beta,long t1)
    {
        int hvalue;
        int move=0;   
        
     	ArrayList <Integer> plist = new ArrayList <Integer>();
	plist=possiblemoves(board);
        if(plist.isEmpty())
        { 
            return 0;
        }
        
        String str = board.toString();
        String[] token = str.split(";");
        
        int[] board1 = new int [14];
        int i,j;
        
        for(i=0;i<14;i++)
        {
            board1[i] = Integer.parseInt(token[i]);
        }
        for(int m:plist)
		{
            long t2=System.currentTimeMillis();
            if((t2-t1)<5000)
            {
                GameState s= (GameState) board.clone();
				s.makeMove(m);
                if(board1[m]==(7-m))
                {
                    hvalue=maxturn(s,md,alpha,beta,t1);
                }
                else
                {
					hvalue=minturn(s,(md-1),alpha,beta,t1);
                }
        if(hvalue>alpha)
		{
			alpha=hvalue;
			move=m;
		}
		if(alpha>=beta)
		{
			return move;
		}
            }
            else break;
	}
	return move;
}
    
    public int minturn(GameState mboard,int md,int alpha,int beta,long t1)
    {
        int hvalue=0;
	if(md<=0)
            return heuristicvalue(mboard);
	ArrayList<Integer> plist = new ArrayList<Integer>();
	plist=possiblemoves(mboard);
        if(plist.isEmpty())
        { 
            return hvalue;
        }
        String s = mboard.toString();
        String[] token = s.split(";");
        
        int[] board = new int [14];
        int i,j;
        
        for(i=0;i<14;i++)
        {
            board[i] = Integer.parseInt(token[i]);
        }
	for(int m:plist)
	{
            long t2=System.currentTimeMillis();
            if((t2-t1)<5000)
            {
		GameState gss= (GameState) mboard.clone();
		gss.makeMove(m);
                if(board[m+7]==(7-m))
                {
                    hvalue=minturn(gss,md,alpha,beta,t1);
                }
                else
                {
		hvalue=maxturn(gss,(md-1),alpha,beta,t1);
                }
               
		if(hvalue<beta)
		{
			beta=hvalue;
		}
		if(alpha>=beta)
		{
			return beta;
		}
            }
            else break;
	}
	return beta;
    }
    
    public int maxturn(GameState nboard,int md,int alpha,int beta,long t1)
{
     int hvalue=0;
     
	if(md<=0)
            return heuristicvalue(nboard);
	ArrayList<Integer> list = new ArrayList<Integer>();
	list=possiblemoves(nboard);
        if(list.isEmpty())
        {
            return hvalue;
        }
        String s = nboard.toString();
        String[] token = s.split(";");
        
        int[] board = new int [14];
        int i,j;
        
        for(i=0;i<14;i++)
        {
            board[i] = Integer.parseInt(token[i]);
        }
	for(int n: list)
	{
            long t2=System.currentTimeMillis();
            if((t2-t1)<5000)
            {
		GameState gs= (GameState) nboard.clone();
		gs.makeMove(n);
                if(board[n]==(7-n))
                {
                    hvalue=maxturn(gs,md,alpha,beta,t1);
                }   
                else
                {
		hvalue=minturn(gs,(md-1),alpha,beta,t1);
                }
               
		if(hvalue>alpha)
		{
			alpha=hvalue;
		}
		if(alpha>=beta)
		{
			return alpha;
		}
            }
            else break;
	}
	return alpha;
}
    public int heuristicvalue(GameState gs)
{ 
    int x4=0;
        int x,x1,x2=0,x3=0;
        String s = gs.toString();
        String[] token = s.split(";");
        
        int[] board = new int [14];
        int nextplayer;
        int i;
        
        for(i=0;i<14;i++)
        {
            board[i] = Integer.parseInt(token[i]);
        }
        nextplayer = Integer.parseInt(token[14]);
        
        if(nextplayer == 1)
        {
            x1=(board[7] - board[0])*10;
          
            for(i=1;i<=6;i++)
            {
                x=(7 - i);
                if(board[i]==x)
                    x2 += x*5;
                if(board[i]==13)
                    x3 += 26;
                if(board[i]==0)
                    x4++;
            }
            
            return (x1+x2+x3+x4);
        }
        else
        {
            x1=(board[0] - board[7])*10;
            for(i=8;i<=13;i++)
            {
                x=(14 - i);
                if(board[i]==x)
                    x2 += x*5;
                if(board[i]==13)
                    x3 += 26;
                if(board[i]==0)
                    x4++;
            }
            return (x1+x2+x3+x4);
        }
}
    
    public ArrayList<Integer> possiblemoves(GameState gs)
{
		String s = gs.toString();
        String[] token = s.split(";");
        
        int[] board = new int [14];
        int nextplayer;
        int i,j;
        
        for(i=0;i<14;i++)
        {
            board[i] = Integer.parseInt(token[i]);
        }
        nextplayer = Integer.parseInt(token[14]);
        
 	ArrayList<Integer> pmlist = new ArrayList<Integer>();
        
	if (nextplayer == 1)
        {
            for ( i = GameState.START_S; i <= GameState.END_S; i++)
            {
                if (board[i] > 0) pmlist.add(i);
            }
        }
        else
        {
            for ( i = GameState.START_N; i <= GameState.END_N; i++)
            {
                if (board[i] > 0)
                {
                    j = i-7;
                    pmlist.add(j);
                }
            }
            
        }
	return pmlist;
}
    
    
    /**
     * Returns a random ambo number (1-6) used when making
     * a random move.
     * 
     * @return Random ambo number
     */
    public int getRandom()
    {
        return 1 + (int)(Math.random() * 6);
    }
}