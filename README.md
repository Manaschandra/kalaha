# Kalaha - Game

## Implementation
* Implemented artifical agent that takes necessary actions based on the 
actions of the opponent.

## Execution
* Execute KalahaMain.java in kalaha package to observe the results.

### Note
This was an university project. An unimplmented project without the logic was given.
I implemented only the logic where I used minimax 
with iterative deepening search and alpha-beta pruning algorithms 
which would be in AIClient.java in ai package. Use NetBeans IDE.